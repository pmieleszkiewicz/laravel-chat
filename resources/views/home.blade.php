@extends('layouts.app')

@section('content')
    @auth
        <chat-app :user="{{ Auth::user() }}"></chat-app>
    @endauth
</div>
@endsection
