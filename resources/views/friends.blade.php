@extends('layouts.app')

@section('content')
    <div class="container">
        @if(Session::has('status'))
            <div class="row">
                <div class="alert {{ Session::get('alert-class', 'alert-success') }} col-sm-10 offset-sm-1" role="alert">
                    {{ Session::get('status') }}
                </div>
            </div>
        @endif

        @if(Session::has('invitationAccepted'))
            <div class="row">
                <div class="alert alert-success col-sm-10 offset-sm-1" role="alert">
                    {{ Session::get('invitationAccepted') }}
                </div>
            </div>
        @endif

        <div class="row" style="margin-bottom: 25px;">
            <div class="list-group col-sm-10 offset-sm-1 no-padding">
                <div class="list-group-item active">Add a new friend</div>
                <div class="list-group-item">
                    <form method="POST" action="{{ route('inviteFriend') }}">
                        <div class="input-group mb-3">
                            {{ csrf_field() }}
                            <input type="email" class="form-control" placeholder="Friend's email" name="email" required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Send invitation</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="row" style="margin-bottom: 25px;">
            <ul class="list-group col-sm-10 offset-sm-1 no-padding">
                <li class="list-group-item active">Friend's invitations</li>
                @if ($friendsWhoInvitedMe->isEmpty())
                    <li class="list-group-item">No friend requests</li>
                @endif
                @foreach ($friendsWhoInvitedMe as $friend)
                    <li class="list-group-item">
                        {{ $friend->getFullName() }}
                        <form method="POST" action="{{ route('acceptInvitation', ['id' => $friend->id]) }}" style="margin: 0; padding: 0; display: inline;">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-success float-right">Accept</button>
                        </form>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="row">
            <ul class="list-group col-sm-10 offset-sm-1 no-padding">
                <li class="list-group-item active">My friends</li>
                @if ($friends->isEmpty())
                    <li class="list-group-item">No friends</li>
                @endif
                @foreach ($friends as $friend)
                    <li class="list-group-item">{{ $friend->getFullName() }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection
