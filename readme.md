# Laravel Chat App

Real time chat using Laravel 5.6, VueJS 2, Pusher, Laravel Echo.

## Live demo

[Live demo](http://pm-laravel-chat.herokuapp.com)


### Sample credentials

Email: johndoe@example.com  
Password: Secret123

Email: janedoe@example.com  
Password: Secret123