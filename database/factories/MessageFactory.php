<?php

use App\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {
    do {
        $from = mt_rand(1,15);
        $to= mt_rand(1,15);
    } while($from === $to);

    return [
        'from' => $from,
        'to' => $to,
        'message' => $faker->sentence
    ];
});
