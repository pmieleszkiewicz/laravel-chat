<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 15) as $i) {
            do {
                $user_id = mt_rand(1,15);
                $friend_id= mt_rand(1,15);
            } while($user_id === $friend_id);

            DB::table('friends')->insert([
                [
                    'user_id' => $user_id,
                    'friend_id' => $friend_id,
                    'accepted' => true
                ]
            ]);
        }
    }
}
