<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = 'test';

        DB::table('users')->insert([
            [
                'first_name' => 'Maweł',
                'last_name' => 'Pieleszkiewicz',
                'email' => 'pawelmieleszkiewicz@gmail.com',
                'password' => bcrypt($password),
                'api_token' => str_random(64),
                'profile_photo' => 'http://via.placeholder.com/150x150'
            ]
        ]);

        factory(App\User::class, 14)->create();
    }
}