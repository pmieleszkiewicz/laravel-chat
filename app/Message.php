<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'from', 'to', 'message'
    ];

    // disable updated_at (could use Resources API)
    public function setUpdatedAtAttribute($value) {}
}
