<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'api_token', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    // friendship that I started
    function friendsOfMine()
    {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id')
            ->withPivot('accepted');
    }

    // friendship that I was invited to
    function friendOf()
    {
        return $this->belongsToMany('App\User', 'friends', 'friend_id', 'user_id')
            ->withPivot('accepted');
    }

    // accessor allowing you call $user->friends
    public function getFriendsAttribute()
    {
        if ( !array_key_exists('friends', $this->relations))
            $this->loadFriends('friends');

        return $this->getRelation('friends');
    }

    // accessor allowing you call $user->friends
    public function getFriendsNotAcceptedAttribute()
    {
        if ( !array_key_exists('friendsNotAccepted', $this->relations))
            $this->loadFriends('friendsNotAccepted', $accepted = false);

        return $this->getRelation('friendsNotAccepted');
    }

    public function getFriendsWhoInvitedMeAttribute()
    {
        if ( !array_key_exists('friendsWhoInvitedMe', $this->relations))
            $this->loadFriends('friendsWhoInvitedMe', false, true);

        return $this->getRelation('friendsWhoInvitedMe');
    }

    protected function loadFriends($relationName, $accepted = true, $invitedBy = null)
    {
        if ( !array_key_exists($relationName, $this->relations))
        {
            $friends = $this->mergeFriends($accepted, $invitedBy);

            $this->setRelation($relationName, $friends);
        }
    }

    protected function mergeFriends($accepted, $invitedBy)
    {
        $friendsOfMine = $this->friendsOfMine
            ->where('pivot.accepted', '=', $accepted);

        $friendOf = $this->friendOf
            ->where('pivot.accepted', '=', $accepted);

        if ($invitedBy) {
            return $friendOf;
        } elseif ($invitedBy === false) {
            return $friendsOfMine;
        } else {
            return $friendsOfMine->merge($friendOf);
        }
    }
}
