<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getFriends($id)
    {
        $user = Auth::guard('api')->user();
        if (is_null($user))
            return response()->json('Unauthorized', 401);

        return $user->friends->each(function($item, $key) {
            $item->makeHidden(['api_token']);
        });;
    }
}
