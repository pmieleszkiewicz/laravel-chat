<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Http\Requests\CreateMessageRequest;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function createMessage(CreateMessageRequest $request)
    {
        $user = Auth::guard('api')->user();
        if (is_null($user))
            return response()->json('Unauthorized', 401);

        $message = Message::create([
            'from' => $user->id,
            'to' => $request->to,
            'message' => $request->message
        ]);

        broadcast(new NewMessage($message));

        return $message;
    }

    public function getConversationMessages($id)
    {
        $user = Auth::guard('api')->user();
        if (is_null($user))
            return response()->json('Unauthorized', 401);

        $conversationMessages = Message::where([
            ['to', '=', $user->id],
            ['from', '=', $id]
        ])->orWhere([
            ['to', '=', $id],
            ['from', '=', $user->id]
        ])->get();

        return $conversationMessages;
    }
}
