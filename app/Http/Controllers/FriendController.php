<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FriendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $friends = Auth::user()->friends;
        $friendsWhoInvitedMe = Auth::user()->friendsWhoInvitedMe;

        return view('friends', ['friends' => $friends, 'friendsWhoInvitedMe' => $friendsWhoInvitedMe]);
    }

    public function update($id)
    {
        DB::table('friends')->where([
            ['user_id', '=', $id],
            ['friend_id', '=', Auth::id()]
        ])->update(['accepted' => true]);

        Session::flash('invitationAccepted', 'Friend request accepted!');
        return back();
    }

    public function invite(Request $request)
    {
        // Check if user exists
        $user = User::where('email', $request->email)->first();
        if (is_null($user) || $user->id === Auth::id()) {
            $request->session()->flash('status', 'User not found!');
            $request->session()->flash('alert-class', 'alert-danger');
            return back();
        }

        // Check if invitation has been sent
        $friends = DB::table('friends')->where([
            ['user_id', '=', $user->id],
            ['friend_id', '=', Auth::id()]
        ])->orWhere([
            ['user_id', '=', Auth::id()],
            ['friend_id', '=', $user->id]
        ])->get();

        if ($friends->isNotEmpty()) {
            $request->session()->flash('status', 'User has been already invited!');
            $request->session()->flash('alert-class', 'alert-danger');
            return back();
        }

        DB::table('friends')->insert([
            'user_id' => Auth::id(),
            'friend_id' => $user->id
        ]);

        $request->session()->flash('status', 'Invitation has been sent!');
        return back();
    }
}
